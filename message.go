package go_logger

import (
	"fmt"
	"time"
)

type MessageType rune

const (
	TypeError   MessageType = 'E'
	TypeWarning MessageType = 'W'
	TypeInfo    MessageType = 'I'
	TypeDebug   MessageType = 'D'
)

type Message struct {
	Tag       string
	Content   string
	Type      MessageType
	TimeStamp time.Time
}

func (m *Message) Format(format string) string {
	year, month, day := m.TimeStamp.Date()
	hour, minute, sec := m.TimeStamp.Clock()
	weekday := m.TimeStamp.Weekday()
	nsec := m.TimeStamp.Nanosecond()
	msec, mcsec := nsec/1000000, nsec/1000

	result := ""
	runes := []rune(format)
	for i := 0; i < len(runes); i++ {
		it := runes[i]
		if it == '%' {
			i++
			if i >= len(runes) {
				break
			}

			it = runes[i]
			switch it {
			case '%':
				result += "%"

			case 'd':
				result += fmt.Sprintf("%02d", day)

			case 'w':
				result += weekday.String()

			case 'W':
				result += weekday.String()[:3]

			case 'm':
				result += fmt.Sprintf("%02d", int(month))

			case 'o':
				result += month.String()

			case 'O':
				result += month.String()[:3]

			case 'y':
				result += fmt.Sprintf("%02d", year%100)

			case 'Y':
				result += fmt.Sprintf("%04d", year)

			case 'h':
				h := hour
				postfix := "AM"
				if h > 12 {
					h -= 12
					postfix = "PM"
				}
				result += fmt.Sprintf("%02d %s", h, postfix)

			case 'H':
				result += fmt.Sprintf("%02d", hour)

			case 'M':
				result += fmt.Sprintf("%02d", minute)

			case 'S':
				result += fmt.Sprintf("%02d", sec)

			case 'f':
				result += fmt.Sprintf("%06d", mcsec)

			case 'F':
				result += fmt.Sprintf("%03d", msec)

			case 'n':
				result += fmt.Sprintf("%09d", nsec)

			case 't':
				result += string(m.Type)

			case 'T':
				result += m.Tag

			case 'c':
				result += m.Content

			default:
				result += "%" + string(it)
			}
		} else {
			result += string(it)
		}
	}

	return result
}

type MessageWriter interface {
	writeMessage(m Message, format string) error
}
