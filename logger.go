package go_logger

import (
	"io"
	"time"
)

/** example:
 * Date: 10 Aug 2020
 * Time: 14:50:15, milliseconds: 777
 * Message Tag: "MyTag"
 * Message Content: "It's my Message"
 * Message Type: "Info"
 * Result: "2020.08.10, 14:50:15:777 MyTag/I: It's my Message"
 */
const DefaultMessageFormat = "%Y.%m.%d, %H:%M:%S:%F %T/%t: %c"

type Logger struct {
	Tag           string
	MessageFormat string
	Output        MessageWriter
	DebugMode     bool
}

func New(tag string, debug bool) *Logger {
	return &Logger{
		Tag:           tag,
		MessageFormat: DefaultMessageFormat,
		Output:        STDOUT,
		DebugMode:     debug,
	}
}

func (l *Logger) SetOutputWriter(w io.Writer) {
	l.Output = &ioMessageWriter{out: w}
}

func (l *Logger) message(t MessageType, message string, ts time.Time) {
	if err := l.Output.writeMessage(Message{
		Tag:       l.Tag,
		Content:   message,
		Type:      t,
		TimeStamp: ts,
	}, l.MessageFormat); err != nil {
		panic("Logger <" + l.Tag + ">: cannot write message -> " + err.Error())
	}
}

func (l *Logger) Error(message string) {
	l.message(TypeError, message, time.Now())
}

func (l *Logger) Fatal(message string) {
	l.message(TypeError, message, time.Now())
	panic("Logger <" + l.Tag + ">: fatal message -> " + message)
}

func (l *Logger) Warning(message string) {
	l.message(TypeWarning, message, time.Now())
}

func (l *Logger) Info(message string) {
	l.message(TypeInfo, message, time.Now())
}

func (l *Logger) Debug(message string) {
	if l.DebugMode {
		l.message(TypeDebug, message, time.Now())
	}
}

// short aliases
func (l *Logger) E(message string) {
	l.Error(message)
}

func (l *Logger) F(message string) {
	l.Fatal(message)
}

func (l *Logger) W(message string) {
	l.Warning(message)
}

func (l *Logger) I(message string) {
	l.Info(message)
}

func (l *Logger) D(message string) {
	l.Debug(message)
}
