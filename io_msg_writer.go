package go_logger

import (
	"errors"
	"io"
	"os"
	"strconv"
)

type ioMessageWriter struct {
	out io.Writer
}

func (w *ioMessageWriter) writeMessage(m Message, format string) error {
	msg := []byte(m.Format(format) + "\n")
	if n, err := w.out.Write(msg); err != nil {
		return err
	} else if n != len(msg) {
		return errors.New("cannot write all " + strconv.Itoa(len(msg)) + " bytes")
	} else {
		return nil
	}
}

var STDOUT = &ioMessageWriter{out: os.Stdout}
var STDERR = &ioMessageWriter{out: os.Stderr}
