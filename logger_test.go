package go_logger

import (
	"bytes"
	"testing"
)

type virtualFile struct {
	content bytes.Buffer
}

func (v *virtualFile) Write(p []byte) (n int, err error) {
	return v.content.Write(p)
}

func TestLogger_D(t *testing.T) {
	tests := []struct {
		name     string
		debug    bool
		message  string
		expected string
	}{
		{
			name:     "debug",
			debug:    true,
			message:  "my message",
			expected: "debug/D: my message",
		},
		{
			name:     "no-debug",
			debug:    false,
			message:  "my message",
			expected: "",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			l := New(test.name, test.debug)
			l.MessageFormat = "%T/%t: %c"
			out := &virtualFile{}
			l.SetOutputWriter(out)
			l.D(test.message)
			if got := out.content.String(); got != test.expected {
				t.Errorf("Logger_D() got = %q, what %q", got, test.expected)
			}
		})
	}
}

func TestLogger_E(t *testing.T) {
	tests := []struct {
		name     string
		debug    bool
		message  string
		expected string
	}{
		{
			name:     "debug",
			debug:    true,
			message:  "my message",
			expected: "debug/E: my message",
		},
		{
			name:     "no-debug",
			debug:    false,
			message:  "my message",
			expected: "no-debug/E: my message",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			l := New(test.name, test.debug)
			l.MessageFormat = "%T/%t: %c"
			out := &virtualFile{}
			l.SetOutputWriter(out)
			l.E(test.message)
			if got := out.content.String(); got != test.expected {
				t.Errorf("Logger_E() got = %q, what %q", got, test.expected)
			}
		})
	}
}

func TestLogger_F(t *testing.T) {
	tests := []struct {
		name     string
		debug    bool
		message  string
		expected string
	}{
		{
			name:     "debug",
			debug:    true,
			message:  "my message",
			expected: "debug/D: my message",
		},
		{
			name:     "no-debug",
			debug:    false,
			message:  "my message",
			expected: "",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			l := New(test.name, test.debug)
			l.MessageFormat = "%T/%t: %c"
			out := &virtualFile{}
			l.SetOutputWriter(out)
			defer func() {
				if err := recover(); err != nil {
					if got, ok := err.(string); ok {
						if got != "Logger <"+l.Tag+">: fatal message -> "+test.message {
							t.Errorf("Logger_F() got = %q, what %q", got, test.expected)
						}
					} else {
						t.Errorf("Logger_F() expected panic(message string)")
					}
				} else {
					t.Errorf("Logger_F() expected panic")
				}
			}()
			l.F(test.message)
		})
	}
}

func TestLogger_I(t *testing.T) {
	tests := []struct {
		name     string
		debug    bool
		message  string
		expected string
	}{
		{
			name:     "debug",
			debug:    true,
			message:  "my message",
			expected: "debug/I: my message",
		},
		{
			name:     "no-debug",
			debug:    false,
			message:  "my message",
			expected: "no-debug/I: my message",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			l := New(test.name, test.debug)
			l.MessageFormat = "%T/%t: %c"
			out := &virtualFile{}
			l.SetOutputWriter(out)
			l.I(test.message)
			if got := out.content.String(); got != test.expected {
				t.Errorf("Logger_I() got = %q, what %q", got, test.expected)
			}
		})
	}
}

func TestLogger_W(t *testing.T) {
	tests := []struct {
		name     string
		debug    bool
		message  string
		expected string
	}{
		{
			name:     "debug",
			debug:    true,
			message:  "my message",
			expected: "debug/W: my message",
		},
		{
			name:     "no-debug",
			debug:    false,
			message:  "my message",
			expected: "no-debug/W: my message",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			l := New(test.name, test.debug)
			l.MessageFormat = "%T/%t: %c"
			out := &virtualFile{}
			l.SetOutputWriter(out)
			l.W(test.message)
			if got := out.content.String(); got != test.expected {
				t.Errorf("Logger_W() got = %q, what %q", got, test.expected)
			}
		})
	}
}
